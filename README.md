# get_next_line - @WeThinkCode_

A C function that reads any valid file line by line until the end.


### What is get_next_line?

[get_next_line](get_next_line.en.pdf) is an individual project at @WeThinkCode_ that basically reads a file line by line.

Disclaimer: *There are so many easier methods of doing this by using standard C functions. But the goal here is to be able to do it by using any functions from my libft and only the standard functions `read`, `malloc` and `free`.*

### Why would I use/try it?

You probably will never have to use this function if you are not a WeThinkCode_ student. The goal is to get better at C. As I said above, you can only use those three standard library functions:

* read
* malloc
* free

So it makes the project harder. But you can also use functions from your personal library.

After finishing this project, you'll definitely learn some more about static variables, pointers, arrays, linked lists (if you decide to do the bonus part), dynamic memory allocation and file manipulation.

My code is not the best, but it passed all the 42 tests successfully.

### How do I use it?
```
$> make -C libft/ fclean && make -C libft/

$> clang -Wall -Wextra -Werror -I libft/includes -o get_next_line.o -c get_next_line.c
$> clang -Wall -Wextra -Werror -I libft/includes -o main.o -c main.c
$> clang -o test_gnl main.o get_next_line.o -I libft/includes -L libft/ -lft
```