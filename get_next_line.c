/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mndlovu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/14 13:15:29 by mndlovu           #+#    #+#             */
/*   Updated: 2017/06/24 15:01:46 by mndlovu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static	int			ft_read(int const fd, char **holder)
{
	char			*buff;
	char			*temp;
	int				ret;

	if (!(buff = (char *)malloc(sizeof(char) * (BUFF_SIZE + 1))))
		return (-1);
	ret = read(fd, buff, BUFF_SIZE);
	if (ret > 0)
	{
		buff[ret] = '\0';
		temp = ft_strjoin(*holder, buff);
		free(*holder);
		*holder = temp;
	}
	free(buff);
	return (ret);
}

int					get_next_line(const int fd, char **line)
{
	static char		*holder = NULL;
	char			*line_feed;
	int				results;

	if ((!holder && (holder = (char *)malloc(sizeof(char))) == NULL) || !line
			|| fd < 0 || BUFF_SIZE < 0)
		return (-1);
	line_feed = ft_strchr(holder, '\n');
	while (line_feed == NULL)
	{
		results = ft_read(fd, &holder);
		if (results == 0)
		{
			if (ft_strlen(holder) == 0)
				return (0);
			holder = ft_strjoin(holder, "\n");
		}
		if (results < 0)
			return (-1);
		else
			line_feed = ft_strchr(holder, '\n');
	}
	*line = ft_strsub(holder, 0, ft_strlen(holder) - ft_strlen(line_feed));
	holder = ft_strdup(line_feed + 1);
	return (1);
}
